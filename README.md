Feeel Legacy
============

Feeel Legacy is an open-source Android app for doing simple at-home exercises.

Since it was released, it has been completely rewritten in Flutter. **[Go follow development at its new repository!](https://gitlab.com/enjoyingfoss/feeel)**

Donate
====
If you'd like to support the development of Feeel, please contribute through **[Liberapay](https://liberapay.com/Feeel/)**.

Install
====

For those who are stuck with older versions of Android, you can find the Legacy app on [F-Droid](https://f-droid.org/packages/com.enjoyingfoss.feeel/).

For everyone else, check out [the new version of Feeel](https://gitlab.com/enjoyingfoss/feeel).