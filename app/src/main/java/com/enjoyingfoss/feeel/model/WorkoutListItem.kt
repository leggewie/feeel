/*
 * This file is part of Feeel.
 *
 *     Feeel is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Feeel is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Feeel.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.enjoyingfoss.feeel_legacy.model

import android.os.Parcel
import android.os.Parcelable

class WorkoutListItem(val id: Int,
                      val titleResource: Int?,
                      val title: String?,
                      val color: Long,
                      val breakLength: Int) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readInt(),
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readString(),
            parcel.readLong(),
            parcel.readInt()) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(id)
        parcel.writeValue(titleResource)
        parcel.writeString(title)
        parcel.writeLong(color)
        parcel.writeInt(breakLength)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<WorkoutListItem> {
        override fun createFromParcel(parcel: Parcel): WorkoutListItem {
            return WorkoutListItem(parcel)
        }

        override fun newArray(size: Int): Array<WorkoutListItem?> {
            return arrayOfNulls(size)
        }
    }

}